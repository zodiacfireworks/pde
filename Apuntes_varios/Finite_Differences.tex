\documentclass[10pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Finite Differences}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}

\section{Introduction}

Finite differences are linear operators (matrices) that act on finite arrays and try to approximate derivative operators acting on functions, that is, on infinite dimensional vectors. 
So the first step is to represent the functions as finite vector by using some of its values, as distributed on a finite array on the domain of dependence. In the simplest case that array consists of evenly spaced points in the domain, but this is not the most efficient method, but it is the simplest.

Thus we take an array $\{x_{i} = a + i * dx\}, \;\; i = 0, \ldots N-1$, $dx = \frac{b-a}{N-1}$ of grid points, and we denote
by $u_i := u(x_i)$ the image of $u$ by the grid map. 

\section{The simplest finite difference operators approximating $\frac{d}{dx}$}

We define:

\[
(D_{\pm}u)_i := \frac{\pm (u_{i\pm1} - u_i)}{dx} 
\]
as the approximations to the limits to the left (right) of the derivative definition. 

How big is the error we make with these approximations? We can answer this by assuming $u$ is smooth and using Taylor's series,
Expanding $u$ we get, 

\[
u_{i\pm1} = u_i  \pm \frac{du}{dx}|_{x_i}  dx +  \frac{d^2u}{dx^2}|_{x_i} \frac{dx^2}{2}  \pm  \frac{d^3u}{dx^3}|_{x_i} \frac{dx^3}{6} + \ldots,
\]
and so,
\[
(D_{\pm}u)_i := \frac{\pm (u_{i\pm1} - u_i)}{dx} =  \frac{du}{dx}|_{x_i} \pm \frac{d^2u}{dx^2}|_{x_i} \frac{dx}{2} + O(dx^2).
\]
Thus, if our solutions are smooth, and $dx$ is small enough, then these differences are good approximations. 
But it is easy to get a much better approximation by exploiting the fact that terms come with alternating signs, by taking the average of these two approximations we find a better one:

\[
(D_0 u)_i := \frac{(D_{+}u)_i + (D_{+}u)_i}{2} =  \frac{du}{dx}|_{x_i} + \frac{d^3u}{dx^3}|_{x_i} \frac{dx^2}{6} + O(dx^3)
\]
This operador, called the center finite difference operator is a much convenient one, not only for its precision, but also --as we shortly see-- for the properties of its eigenvalues. 

Notice that if we apply these operators to a polynomial $x^n$ we get that the first two are exact for $n=0, 1$ (for all higher order derivatives in the error expression vanish), while the centered operator is exact even for the $n=2$ case. 
So, one easy way to check the accuracy of the operators, or even to find them, is to apply them to polynomials of growing order. 

\section{Higher accuracy operators}

It is easy to find finite difference operators with smaller errors, for instance, to find the centered finite difference operator of $4^{th}$ order we define it with free coefficients and impose the accuracy, either via Taylor's expansions or using polynomials,

\[
(D_4 u)_i := \frac{a u_{i+2} + b u_{i+1} - b u_{i-1} - a u_{i-2} }{dx}
\]
We have taken the symmetric case for it already guarantees that all even polynomials centered at $x_i$ would vanish.
Thus we need to try with the first and third order polynomials. These two conditions fix the values of $a$ and $b$. 
So we apply it to $u_i = i * dx $, and $u_i = i^3 * dx^3$. In the first case, since the derivative is $1$ we would need to get also $1$, while in the second we would impose it to vanish. 

\begin{eqnarray}
(D_4  i * dx)_i &=& a 2 + b + b + a 2 = 1 \\
(D_4  (i * dx)^3)_i &=& a 2^3 + b + b + a 2^3 = 0 
\end{eqnarray}

From where we see that $(a=\frac{-1}{12}, b = \frac{2}{3})$

\[
(D_4 u)_i := \frac{-u_{i+2} + 8 u_{i+1} - 8 u_{i-1} + u_{i-2} }{12dx}
\]

\section{Finite Difference Operators in the Circle}

So far we have considered local approximations to the derivative operator, we now specialize to the case where the domain is a circle of length $L$. That is we consider periodic functions of period $L$.
For this case there are no limitations at the beginning or ends of our grid, for we take extra values using the periodicity.
That is, for instance $x_{N} = x_{0}$, $x_{N+1} = x_{1}$, and so on. In particular $u^N = u^0$, etc.

In that case the matrix representation of the operators is as follows:

\[
D_+ = \frac{1}{dx}\left(
\begin{array}{cccc}
-1 & 1 & 0 & \ldots \\
0 & -1 & 1 & \ldots \\
 \vdots & \vdots & \vdots & \vdots \\
 0 & \ldots & -1 & 1 \\
 1 & \ldots & 0 & -1
\end{array}
\right)
\]

\[
D_0 = \frac{1}{dx}\left(
\begin{array}{ccccc}
0 & 1 & 0 & \ldots &-1\\
-1 & 0 & 1 & 0 & \ldots \\
 \vdots & \vdots & \vdots & \vdots & \vdots\\
 0 & \ldots & -1 & 0 & 1 \\
 1 & 0 & \ldots & -1 & 0
\end{array}
\right)
\]


Two aspects of these matrices come up front immediately, first they are sparse, so processing them should not be too costly. Second that while the one for $D_0$ is antisymmetric, the one for $D_+$ is not. The antisymmetry of the second would tell us immediately that is diagonizable and its eigenvalues are imaginary. This has important consequences on stability. 

\subsection{Eigenvectors/Eigenvalues of finite difference operators}

Consider the functions $e^{i2\pi k x}$, periodicity would tell that we can only consider $k$'s so that $kL=n$, that is
an infinite set of $k_n = \frac{n}{L}$. But since we are only considering grid points there immediately comes an extra condition, indeed, since $x_l = dx * l = \frac{L}{N}$ (for the periodic case we have $dx = \frac{L}{N}$ and not $\frac{L}{N-1}$), we have, 

\[
i 2 \pi k_n x_l = i 2 \pi \frac{n}{L} \frac{L}{N} l  = i 2 \pi \frac{n}{N}l 
\]
Thus, the action of $k_n$ is the same as the one of $k_{n+N}$ and so we effectible are limited to finite wave numbers,
\[
k_n = \frac{n}{L}, \;\;\; -N/2 \leq n \leq N/2\;\;\; N \;\;\mbox{even}
\]

Let us now apply these set to our finite difference operators:

\[
(D_{\pm} e^{i2\pi k_n x})_l = \pm \frac{e^{i2\pi k_n x_{l \pm 1}} - e^{i2\pi k_n x_{l}}}{dx} 
= \pm \frac{e^{i2\pi k_n x_{l}}(e^{\pm i2\pi k_n dx}  - 1)}{dx} = \pm \frac{e^{\pm i2\pi k_n dx}  - 1}{dx} e^{i2\pi k_n x_{l}}
\]
% 
where we have used that 
$e^{i2\pi k_n x_{l \pm 1}} =  e^{i2\pi k_n (x_{l} \pm dx)} = e^{i2\pi k_n x_{l}} e^{\pm i2\pi k_n dx}$.

Thus, we see that these functions are all eigenvectors of our operators. Notice that they are complex, in particular with real parts. 

Things get much better if we look at the eigenvalues of $D_0$, from its definition we see that,

\[
(D_0 e^{i2\pi k_n x})_l = \frac{(e^{i2\pi k_n dx}  - 1) - (e^{-i2\pi k_n dx}  - 1)}{2dx} \;e^{i2\pi k_n x_{l}}
				= \frac{e^{i2\pi k_n dx}  - e^{-i2\pi k_n dx}  }{2dx} \;e^{i2\pi k_n x_{l}}
				= \frac{i\sin(2\pi	k_n dx)}{ dx}  \;e^{i2\pi k_n x_{l}}
\]

\textbf{Exercise:} Compute the eigenvalues for the fourth oder operator defined above.

\begin{figure}[htbp]
\begin{center}
%\input{disperions.pdf}
%\input{dispersions.png}
\centerline{\includegraphics[width=8cm, height=6cm]{dispersions.png}}
\caption{Dispersion relations for the different finite difference approximations.}
\label{default}
\end{center}
\end{figure}

The above figure shows the dispersion relations for the different finite difference approximations up to order $dx^8$.
Notice that they approach the linear dispersion relation of the operator $\frac{d}{dx}$ for small wave numbers, but that they depart for larger values. 
In fact, they go to zero for the larger grid wave numbers. 

\section{The advection equation.}

We are now going to solve for an approximate solution to the advection equation, using our finite difference operators.
We consider the equation, 
\[
\partial_t u = a \partial_x u, \;\;\;\;\;\; x \in [0,L]
\]
%
with $a$ real and positive. Given an initial data, $u_0= u_0(x)$ the solution is, $u(t,x) = u_0(at + x)$, so the solution is a shift to the left with speed $-a$.

We are now going to solve for a semi-discrete approximation, 

\[
\partial_t v_i = a (D_p v)_i
\]
%
where $\{v_i(t)\}$ is a grid vector, and $D_p$ is one of our order $p$ finite-difference approximations to the space derivative. 

Since $D_p$ is anti-hermitian, it is diagonalizable and so there exists a base where $D_p$ is diagonal and all its 
eigenvalues are pure imaginary. 
Thus we can expand $v$ as,

\[
v_l = \sum_n v_ n e^{i2\pi k_n x_{l}}, \;\;\;\; k_n = \frac{n}{L},
\]
Thus, the coefficients of $v$ in that base satisfy, 

\[
\partial_t w_n = a \lambda_n w_n
\]
%
For instance, if we are using the second order centered finite difference operator, and taking as initial data $u_0(x) = w_n^0 e^{i2\pi k_n x}$, for some $k_n = n/L$,  which corresponds to a grid vector $v_{nl} = e^{i2\pi k_n x_{l}}$ then we would have, 

\[
\partial_t w_n = a \frac{i\sin(2\pi	k_n dx)}{ dx} w_n
\]

Whose solution is, 

\[
w_n(t)_l = e^{i a \frac{\sin(2\pi k_n dx)}{ dx} t - i2\pi k_n x_l} w^0_n
\]
%
Corresponding to the continuum function 
$u(t,x) = e^{i 2\pi k_n (a \frac{\sin(2\pi k_n dx)}{ 2\pi k_n dx} t -  x)} w^0_n$. 
Thus, corresponding to a wave traveling with speed $a \frac{\sin(2\pi k_n dx)}{ 2\pi k_n dx}$. 
Notice that when $dx \to 0$ keeping $k_n$ fixed, the speed tends to $a$. 



Notice also that their group velocities, $\frac{d\lambda(k)}{dk}$ are 
negative! So that high frequency noice travels into the oposite direction!

Another important result is that their maximum propagation speed is larger than the exact one and of opposite sign.
Thus, hight frequency modes are not at all well represented, travel into the opposite direction and with much larger speeds. We shall see these phenomena  in the numerical examples.

\begin{figure}[htbp]
\begin{center}
%\input{disperions.pdf}
%\input{dispersions.png}
\centerline{\includegraphics[width=8cm, height=6cm]{face-velocity.png}}
\caption{Face velocities for the different approximations.}
\label{default}
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
%\input{disperions.pdf}
%\input{dispersions.png}
\centerline{\includegraphics[width=8cm, height=6cm]{group_velocity.png}}
\caption{Group velocities for the different approximations.}
\label{default}
\end{center}
\end{figure}

\section{Fourier Interpolation}

For numerical approximations one does not uses Fourier Series, but rather Fourier Interpolation. This is natural for we do not have at our disposal the values of functions at all points, but rather at a finite number of them.

Consider any periodic function $f(x)$ in $[0,L]$, and a grid, that for convenience we shall take to have an even number of points, $x_l = dx * l,\; l=0, \dots, 2M$, $dx = \frac{L}{2M+1}$. Can we represent as a linear combination o our base vectors,
$\{ e^{i2\pi k_n x_{l}} \}$? Namely, does there exist another vector $\tilde{f}_m$ such that,

\[
f_l := f(x_l) = \sum_{m = -M}^{M} \tilde{f}_m e^{i2\pi k_m x_{l}}.
\]

The answer is rather trivial, since they are eigenvectors of an anti-hermitian matrix they expand the space, so, as a matrix, its determinant is non-vanishing. Even more, they are ortogonal among each other, so, we can invert it easily. 
Notice that,

\[
\frac{1}{L}\sum_{l=0}^{2M}  e^{-i2\pi k_m x_{l}} e^{i2\pi k_n x_{l}} \;dx = \left\{
\begin{array}{c}
	1 \;\;\; n=m \\
	0 \;\;\; n \neq m
\end{array}
\right.
\]

Where we have used that,
\[
\sum_{l=0}^{2M}  = 2M+1 = \frac{L}{dx},
\]
%
and,
\begin{eqnarray*}
\sum_{l=0}^{2M} e^{-i2\pi (k_m-k_n) x_{l}} &=& \sum_{l=0}^{2M} e^{-i2\pi \frac{(m-n)}{2M} l} \\
								    &=& \sum_{l=0}^{2M} (e^{-i2\pi \frac{(m-n)}{2M}})^l \\
				&=& \frac{1-(e^{-i2\pi \frac{(m-n)}{2M+1}})^{2M+1}}{1-(e^{-i2\pi \frac{(m-n)}{2M+1}})}\\
				&=& 0.
\end{eqnarray*}



\end{document}